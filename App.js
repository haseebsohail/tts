import * as React from 'react';
import { Text, View, StyleSheet ,TouchableOpacity,TextInput ,useState} from 'react-native';
import Constants from 'expo-constants';
import * as Speech from 'expo-speech';


// You can import from local files

// or any pure javascript modules available in npm

export default function App() {
  const [number, onChangeNumber] = React.useState(null);

  const runSpeech = () => {
    let phrase = number;
    Speech.speak(phrase, {
      language: "ar-SA"
    })
  }
  
  return (
    <View style={styles.container}>
       <TextInput
        style={styles.input}
        value={number}
        placeholder="useless placeholder"
        keyboardType="numeric"
      />
      <TouchableOpacity onPress={runSpeech}>
      <Text style={styles.paragraph}>
speech
      </Text>
      </TouchableOpacity>
    
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#fff',
    padding: 8,
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
});
